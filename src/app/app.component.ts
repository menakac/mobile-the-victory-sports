import {Component, NgZone, ViewChild} from '@angular/core';
import {AlertController, MenuController, Nav, NavController, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AdminService, appOrders, GlobalData, MainConfig, ServiceConfig, UserService} from "./core";
import { Content } from 'ionic-angular';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  @ViewChild(Content) content: Content;
  currentYear = new Date;
  rootPage:any;
  showpayment:boolean=false;

  constructor(private platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private globalData: GlobalData,
              public userService: UserService, private menuCtrl: MenuController, private adminService: AdminService,
              public appOrders: appOrders, public alertCtrl:AlertController, public zone: NgZone) {

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.initializeApp();
    });


  }

  private initializeApp(){
    Promise.all([
      this.initPlatform(),
      this.initShop(),
      this.initBranch()
    ]).then(()=>{
      this.initUser();
    },()=>{});



  }

  private initPlatform(){
    let promise = new Promise(resolve => {
      let data:any = {};
      if (this.platform.is('tablet')) {
        data = {
          "channel": MainConfig.CHANNELS.TAB.id,
          "serviceAPI": ServiceConfig.CLIENT_TAB_SERVICES
        }
      }else{
        data = {
          "channel": MainConfig.CHANNELS.MOBILE.id,
          "serviceAPI": ServiceConfig.CLIENT_MOBILE_SERVICES
        }
      }
      this.globalData.setDevice(data).then(()=>{
        resolve();
      });
    });
    return promise;
  }

  private initShop(){
    let promise = new Promise((resolve, reject) => {
      this.adminService.getShopDetails().then((response:any) => {
        if(!(response==null) && response.data.length>0){
          this.globalData.setShop(response.data[0]).then(()=>{
            resolve();
          });
        }else{
          reject();
        }
      },() => {
        reject();
      });
    });
    return promise;
  }

  private initBranch(){
    let promise = new Promise((resolve, reject) => {
      this.adminService.getBranchDetails().then((response:any) => {
        if(!(response==null) && response.data.length>0){
          this.globalData.setBranch(response.data[0]).then(()=>{
            resolve();
          });
        }else{
          reject();
        }
      },() => {
        reject();
      });
    });
    return promise;
  }

  initUser(){
    this.userService.getLocalAuthResponse().then((response:any) => {
      if(response.sessionId){
        this.userService.setLocalAuthResponse(response,false).then(()=>{
          this.rootPage = 'HomePage';
        },()=>{
          this.rootPage = 'LoginPage';
        });
      }else{
        this.rootPage = 'LoginPage';
      }
    },() => {
      this.rootPage = 'LoginPage';
    });
  }

  logOut(){
    this.userService.removeLocalAuthResponse().then(()=>{
      this.openPage('LoginPage');
    },()=>{
      this.openPage('LoginPage');
    });
  }

  openPage(page){
    if(this.nav.getActive().name != page){
      this.nav.setRoot(page).then();
    }
    this.menuCtrl.close().then();
  }

  goToPage(page){
    if(this.nav.getActive().name != page){
      this.nav.push(page).then();
    }
    this.menuCtrl.close().then();
  }

  incrementSubmit(e,product) {
    product.quantity=e.value;
    this.appOrders.productToCart(product);
    this.appOrders.calculatePrice();
  }

  clearCart(){
    const confirm = this.alertCtrl.create({
      message: 'Do you agree to clear the cart?',
      buttons: [
        {
          text: 'No',
          handler: () => {

          }
        },
        {
          text: 'Yes',
          handler: () => {
            for (let item of this.appOrders.cartItems){
              item.quantity = 0;
            }
            this.appOrders.cartItems.length = 0;
            this.appOrders.calculatePrice();
          }
        }
      ]
    });
    confirm.present();


  }

  removeProduct(product){
    product.quantity = 0;
    this.appOrders.productToCart(product);
    this.appOrders.calculatePrice();
  }

  gotodetail(product){
    this.nav.push('DetailsPage',{'itemDetails':product});
    this.menuCtrl.close().then();
  }

  morepayment(){

    this.showpayment=!this.showpayment;
    this.zone.run(()=>{
      this.menuCtrl.enable(true,'menu2');

    });

  }

  affixCheck(e){
    this.zone.run(()=>{
      this.menuCtrl.enable(true,'menu2');

    });
  }







}

