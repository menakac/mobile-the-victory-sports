import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ComponentsModule} from "../components/components.module";
import { MyApp } from './app.component';
import { IonicStorageModule } from "@ionic/storage";
import { InquirymodelPage} from "../pages/inquirymodel/inquirymodel";

import {GlobalData, LocalDataService, UserService, HttpService, AdminMap, AdminService, ProductMap, ProductService, appOrders} from "./core";
import {HttpClientModule} from "@angular/common/http";
import {CategoryFiltersPage} from "../pages/category-filters/category-filters";
import {DatePipe} from "@angular/common";


const SERVICES = [
  GlobalData,
  LocalDataService,
  UserService,
  HttpService,
  appOrders
];

const MAPPING_SERVICES = [
  AdminMap,
  ProductMap,
];

const API_DATA_SERVICES = [
  AdminService,
  ProductService,
];

@NgModule({
  declarations: [
    MyApp,
    CategoryFiltersPage,
    InquirymodelPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ComponentsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    InquirymodelPage,
    CategoryFiltersPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatePipe,
    ...SERVICES,
    ...MAPPING_SERVICES,
    ...API_DATA_SERVICES,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
