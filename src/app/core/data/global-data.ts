export class GlobalData {

  public shop : any = {};

  public branch : any = {};

  public device:any= {};

  public setShop(data : any){
    let promise = new Promise(resolve => {
      this.shop = data;
      resolve();
    });
    return promise;
  }

  public setBranch(data : any){
    let promise = new Promise(resolve => {
      this.branch = data;
      resolve();
    });
    return promise;
  }

  public setDevice(data:any){
    let promise = new Promise(resolve => {
      this.device = data;
      resolve();
    });
    return promise;
  }
}
