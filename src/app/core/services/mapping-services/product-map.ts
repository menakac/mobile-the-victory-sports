import {Injectable} from "@angular/core";
import {GlobalData} from "../../data";
import {AppConfig,MainConfig} from "../../config";

@Injectable()
export class ProductMap {
  constructor(private globalData: GlobalData) {}

  mapProductCategory(){
    return AppConfig.shopDetails.shopID || 0;
  }

  public mapFindByCriteriaReq(data){
    let request = {
      "channel": MainConfig.CHANNELS.MOBILE.id || null,
      "shopId": data.shopId || 0,
      "branchId": data.branchId || 0,
      "PKs": data.PKs || [],
      "offset": data.offset || 0,
     "limit": data.limit || 1,
      "searchKeys": data.searchKeys || [],
      "values": data.values || [],
      "operators": data.operators || [],
      "fromDate": data.fromDate || null,
      "toDate": data.toDate || null,
      "orderByKey": data.orderByKey || null,
      "orderByValue": data.orderByValue || null,
      "groupBy": data.groupBy || [],
      "statuses": data.statuses || []
    };
    return request;
  }

  mapCalculatePrice(data){
    let request = {
      "channel": MainConfig.CHANNELS.MOBILE.id ||0,
      "shopId": data.shopId || 0,
      "branchId": data.branchId || 0,
      "quotationId":0,
      "contact":null,
      "contact1":null,
      "email":null,
      "address":null,
      "comments":null,
      "customerId":0,
      "brokerId":0,
      "customerName":null,
      "deliveryOption":0,
      "deliveryLocation":0,
      "deliveryLatitude":0,
      "deliveryLongitude":0,
      "paymentOption":data.paymentOption||0,
      "frequencyId":0,
      "itemList":data.itemList||[],
      "serviceList":[],
      "expectedTime":null,
      "adjustmentFlat":0,
      "adjustmentPercentage":0,
      "pax":0,
      "externalReference":null,
      "charges":[],
      "taxExempted":false,
      "vatExempted":false,
      "otherData":"{}",
      "session":null,
      "shippingCountry":0,
      "shippingCity":null,
      "additionalData":{}
    };
    return request;
  }

  mapPlaceOrder(data){
    let request = {
      "channel": data.channel ||0,
      "shopId": data.shopId || 0,
      "branchId": data.branchId || 0,
      "quotationId": data.quotationId || 0,
      "contact": data.contact||null,
      "contact1": data.contact1 || null,
      "email": data.email||null,
      "address": data.address||null,
      "comments": data.comments||null,
      "customerId": data.customerId||0,
      "brokerId": data.brokerId || 0,
      "customerName": data.customerName||null,
      "deliveryOption": data.deliveryOption || 0,
      "deliveryLocation": data.deliveryLocation || 0,
      "deliveryLatitude": data.deliveryLatitude || 0,
      "deliveryLongitude": data.deliveryLongitude || 0,
      "paymentOption": data.paymentOption || 0,
      "frequencyId": data.frequencyId || 0,
      "itemList": data.itemList || [],
      "serviceList": data.serviceList || [],
      "expectedTime": data.expectedTime || null,
      "adjustmentFlat": data.adjustmentFlat || 0,
      "adjustmentPercentage": data.adjustmentPercentage || 0,
      "pax": data.pax || 0,
      "externalReference": data.externalReference || null,
      "charges": data.charges || [],
      "taxExempted": data.taxExempted || false,
      "vatExempted": data.vatExempted || false,
      "otherData": data.otherData || "{}",
      "session": data.session || null,
      "shippingCountry": data.shippingCountry || 0,
      "shippingCity": data.shippingCity || null,
      "additionalData": data.additionalData || {},
      "additionalColumns": data.additionalColumns || {}
    };

    return request;
  }

}
