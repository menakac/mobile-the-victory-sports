import { Injectable } from '@angular/core';
import {AppConfig, MainConfig} from "../../config";
import {GlobalData} from "../../data";

@Injectable()
export class AdminMap {

  constructor(private globalData: GlobalData) {}

  mapShopDetails(){
    return AppConfig.shopDetails.shopID || 0;
  }

  mapBranchDetails(){
    return AppConfig.shopDetails.branchID || 0;
  }

   mapLoginResponse(data){
      let response = {
        "loginStatus":data.loginStatus || false,
        "message":data.message||null,
        "status":data.status||0,
        "customerId":data.customerId||0,
        "firstName":data.firstName||null,
        "middleName":data.middleName||null,
        "lastName":data.lastName||null,
        "loginName":data.loginName||null,
        "address1":data.address1||null,
        "address2":data.address2||null,
        "city":data.city||null,
        "countryId":data.countryId||0,
        "mobile":data.mobile||null,
        "email":data.email||null,
        "sessionId":data.sessionId||null,
        "dateOfBirth":data.dateOfBirth||null,
        "addresses":data.addresses||[]
      };
      return response;
   }

  mapCreateCustomer(data){
    let request = {
      "channel": this.globalData.device.channel || null,
      "shopId": AppConfig.shopDetails.shopID || 0,
      "branchId": AppConfig.shopDetails.branchID || 0,
      "firstName": data.firstName || null,
      "middleName": data.middleName || null,
      "lastName": data.lastName || null,
      "loginName": data.loginName || null,
      "password": data.password || null,
      "address1": data.address1 || null,
      "address2": data.address2 || null,
      "city": data.city || null,
      "countryId": data.countryId || 0,
      "mobile": data.mobile || null,
      "fax": data.fax || null,
      "email": data.email || null,
      "dateOfBirth": data.dateOfBirth || null
    };
    return request;
  }

  mapResetPassword(data){
    let request = {
      "channel": this.globalData.device.channel || null,
      "shopId": AppConfig.shopDetails.shopID || 0,
      "branchId": AppConfig.shopDetails.branchID || 0,
      "loginName": data.loginName || null,
      "email": data.email || null
    };
    return request;
  }

  mapUpdateCustomer(data){
    let request = {
      "channel": data.channel || 0,
      "shopId": data.shopId || 0,
      "branchId": data.branchId || 0,
      "customerId": data.customerId||0,
      "firstName": data.firstName||'',
      "middleName": data.middleName||'',
      "lastName": data.lastName||'',
      "address1": data.address1||null,
      "address2": data.address2||null,
      "city": data.city||null,
      "countryId": data.countryId||0,
      "mobile": data.mobile||null,
      "fax": data.fax||null,
      "email": data.email||null,
      "dateOfBirth": data.dateOfBirth||''
    };
    return request;
  }

  mapChangePassword(data){
    let request = {
      "channel": data.channel || 0,
      "shopId": data.shopId || 0,
      "branchId": data.branchId || 0,
      "primary_id": data.primary_id || 0,
      "oldPassword": data.oldPassword || null,
      "newPassword": data.newPassword || null,
      "sessionId": data.sessionId || null
    };
    return request;
  }

  mapFindCustomerOrders(data){
    let request = {
      "channel": MainConfig.CHANNELS.MOBILE.id || null,
      "shopId": data.shopId || 0,
      "branchId": data.branchId || 0,
      "PK": data.PK||0,
      "PKs": data.PKs || [],
      "offset": data.offset || 0,
      "limit": data.limit || 1,
      "searchKeys": data.searchKeys || [],
      "values": data.values || [],
      "operators": data.operators || [],
      "fromDate": data.fromDate || null,
      "toDate": data.toDate || null,
      "orderByKey": data.orderByKey || null,
      "orderByValue": data.orderByValue || null,
      "groupBy": data.groupBy || [],
      "statuses": data.statuses || [],
      "preparationPoint": data.preparationPoint||0,
      "servingPoint": data.servingPoint||0,
      "consumingPoint": data.consumingPoint||0
    };
    return request;
  }


}
