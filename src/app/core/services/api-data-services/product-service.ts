import {Injectable} from "@angular/core";
import {HttpService} from "../http.service";
import {GlobalData} from "../../data";
import {AppConfig} from "../../config";
import {ProductMap} from "../mapping-services";

@Injectable()
export class ProductService {
  constructor(private productMap : ProductMap, private httpService : HttpService, private globalData: GlobalData) {}

  //GET
  public getProductCategory(){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpGet( AppConfig.GB_API_URL, this.globalData.device.serviceAPI ,"/productCategories/" + this.productMap.mapProductCategory() , {} ,null, true).then((data : any) => {
        resolve(data);
      }).catch(() => {
        reject(null);
      });
    });
    return promise;
  }

  //POST
  public SearchProductsByCategoryBrand(req){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpPost(AppConfig.GB_API_URL , this.globalData.device.serviceAPI ,"/product/category/brand/findByCriteria", this.productMap.mapFindByCriteriaReq(req) ,null, false).then((data : any) => {
        resolve(data);
      }).catch((error : any) => {
        reject(error);
      });
    });
    return promise;
  }

  //POST
  public calculatePrice(req){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpPost(AppConfig.GB_API_URL , this.globalData.device.serviceAPI ,"/price", this.productMap.mapCalculatePrice(req) ,null, false).then((data : any) => {
          resolve(data);
        }).catch((error : any) => {
        reject(error);
      });
    });
    return promise;
  }


  //GET
  public getProductDetails(req){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpGet(AppConfig.GB_API_URL , this.globalData.device.serviceAPI ,"/productDetails/"+ req.shopId+ "/" + req.productId , {} ,null, false).then((data : any) => {
        resolve(data);
        // resolve(this.dineInOrderMap.mapTableSessions(data));
      }).catch((error : any) => {
        reject(error);
      });
    });
    return promise;
  }

  //POST
  public SubmitInquiry(req){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpPost(AppConfig.GB_API_URL , this.globalData.device.serviceAPI ,"/inquiry", req ,null, false).then((data : any) => {
        resolve(data);
      }).catch((error : any) => {
        reject(error);
      });
    });
    return promise;
  }

  //POST
  public placeOrder(req){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpPost(AppConfig.GB_API_URL , this.globalData.device.serviceAPI ,"/submit", this.productMap.mapPlaceOrder(req) ,null, false).then((data : any) => {
        resolve(data);
      }).catch((error : any) => {
        reject(error);
      });
    });
    return promise;
  }

}
