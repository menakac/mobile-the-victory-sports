import { Injectable } from '@angular/core';
import { AppConfig } from "../../config";
import { HttpService, AdminMap } from "../../services";
import {GlobalData} from "../../data";

@Injectable()
export class AdminService {

  constructor(private adminMap : AdminMap, private httpService : HttpService, private globalData: GlobalData) {}

  //GET
  public getShopDetails(){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpGet( AppConfig.GB_API_URL, this.globalData.device.serviceAPI ,"/shopDetails/" + this.adminMap.mapShopDetails() , {} ,null, true).then((data : any) => {
        resolve(data);
      }).catch(() => {
        reject(null);
      });
    });
    return promise;
  }

  public getBranchDetails(){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpGet( AppConfig.GB_API_URL, this.globalData.device.serviceAPI ,"/branchDetail/" + this.adminMap.mapBranchDetails(), {} ,null, true).then((data : any) => {
        resolve(data);
      }).catch(() => {
        reject(null);
      });
    });
    return promise;
  }


  //PUT
  public login(req){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpPut(AppConfig.GB_API_URL , this.globalData.device.serviceAPI ,"/customer/login", req , null, false).then((data : any) => {
        resolve(this.adminMap.mapLoginResponse(data));
      }).catch(() => {
        reject(null);
      });
    });
    return promise;
  }

  //POST
  public createCustomer(req){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpPost(AppConfig.GB_API_URL , this.globalData.device.serviceAPI ,"/customer", this.adminMap.mapCreateCustomer(req) ,null, false).then((data : any) => {
        resolve(data);
      }).catch((error : any) => {
        reject(error);
      });
    });
    return promise;
  }

  //PUT
  public resetPassword(req){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpPut(AppConfig.GB_API_URL , this.globalData.device.serviceAPI ,"/customer/resetPassword", this.adminMap.mapResetPassword(req) , null, false).then((data : any) => {
        resolve(data);
      }).catch((error : any) => {
        reject(error);
      });
    });
    return promise;
  }


  //GET
  public getCustomerDetails(req){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpGet( AppConfig.GB_API_URL, this.globalData.device.serviceAPI ,"/customer/details/" + req.shopId, {} ,null, true).then((data : any) => {
        resolve(data);
      }).catch((error : any) => {
        resolve(null);
      });
    });
    return promise;
  }


  //PUT
  public updateCustomer(req){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpPut(AppConfig.GB_API_URL , this.globalData.device.serviceAPI ,"/customer", this.adminMap.mapUpdateCustomer(req) , null, false).then((data : any) => {
        resolve(data);
      }).catch((error : any) => {
        resolve(null);
      });
    });
    return promise;
  }

  //PUT
  public changePassword(req){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpPut(AppConfig.GB_API_URL , this.globalData.device.serviceAPI ,"/customer/changePassword", this.adminMap.mapChangePassword(req) , null, false).then((data : any) => {
        resolve(data);
      }).catch((error : any) => {
        resolve(null);
      });
    });
    return promise;
  }

  //POST
  public findCustomerOrders(req){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpPost(AppConfig.GB_API_URL , this.globalData.device.serviceAPI ,"/serviceOrder/findByCriteria", this.adminMap.mapFindCustomerOrders(req) ,null, false).then((data : any) => {
        resolve(data);
      }).catch((error : any) => {
        reject(error);
      });
    });
    return promise;
  }

  //GET
  public getCountries(){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpGet( AppConfig.GB_API_URL, this.globalData.device.serviceAPI ,"/countries", {} ,null, true).then((data : any) => {
        resolve(data);
      }).catch((error : any) => {
        resolve(null);
      });
    });
    return promise;
  }

  //GET
  public getCities(){
    let promise = new Promise((resolve, reject) => {
      return this.httpService.httpGet( AppConfig.GB_API_URL, this.globalData.device.serviceAPI ,"/cities", {} ,null, true).then((data : any) => {
        resolve(data);
      }).catch((error : any) => {
        resolve(null);
      });
    });
    return promise;
  }
}



