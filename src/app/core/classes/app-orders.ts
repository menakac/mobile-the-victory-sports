import {Injectable} from "@angular/core";
import {ProductService} from "../services/api-data-services";
import {AppConfig, MainConfig} from "../config";
import {GlobalData} from "../data";

@Injectable()
export class appOrders {
  cartItems:any = [];
  orderDetails:any = {};

  public orderSummary:any={};

  constructor(private productService: ProductService, private globalData:GlobalData){}

  productToCart(product){
    this.cartCheck(product.productId).then((key:string)=>{
      if(key == 'noKey' && JSON.parse(product.quantity) !== 0){
        this.cartItems.push(product);
      }else if(key !== 'noKey' && JSON.parse(product.quantity) == 0){
        this.cartItems.splice(key, 1);
      }else if(key !== 'noKey' && JSON.parse(product.quantity) !== 0){
        this.cartItems[key] = product;
      }
    });
  }

  cartCheck(id){
    let myKey='noKey';
    let promise = new Promise(resolve => {
      if(this.cartItems.length > 0){
        for(let key in this.cartItems){
          if(this.cartItems[key].productId == id){
            myKey = key;
          }
        }
        resolve(myKey);
      }else{
        resolve(myKey);
      }
    });
    return promise;
  }

  itemLoadingCartCheck(id){
    let quantity;
    for(let key in this.cartItems){
      if(this.cartItems[key].productId == id){
        quantity = this.cartItems[key].quantity;
        return quantity;
      }
    }
  }

  calculatePrice(){
    if(this.cartItems && this.cartItems.length > 0){
      let req:any = {
        "shopId" : AppConfig.shopDetails.shopID,
        "branchId" : AppConfig.shopDetails.branchID,
        "itemList" : []
      };
      for(let product of this.cartItems){
        let productItem = {
          "productId": product.productId,
          "quantity": product.quantity
        };
        req.itemList.push(productItem);
      }
      this.productService.calculatePrice(req).then((response:any)=>{
        if(response && response.status == MainConfig.responseSuccessStatus){
          this.orderDetails = response.orderDetails;
        }
      });
    }else{
      this.orderDetails = {};
    }
  }

  placeOrder(req:any){
    let promise = new Promise((resolve, reject) => {
      req.channel = this.globalData.device.channel;
      req.shopId = AppConfig.shopDetails.shopID;
      req.branchId = AppConfig.shopDetails.branchID;
      req.itemList = [];
      for(let product of this.cartItems){
        let productItem = {
          "productId": product.productId,
          "quantity": product.quantity
        };
        req.itemList.push(productItem);
      }

      this.productService.placeOrder(req).then((data:any)=>{
        if(data && data.status == MainConfig.responseSuccessStatus){
          resolve(data.orderDetails);
        }else{
          reject();
        }
      },()=>{
        reject();
      });
    });

    return promise;
  }
}
