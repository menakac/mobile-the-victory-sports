export class ServiceConfig {

  public static CLIENT_MOBILE_SERVICES = {
    NAME : 'Client mobile services',
    ROUTE_PATH : '/api/mobile'
  };

  public static CLIENT_TAB_SERVICES = {
    NAME : 'Client tab services',
    ROUTE_PATH : '/api/tab'
  };

}
