export class MainConfig {
  public static responseSuccessStatus = 1;

  public static CHANNELS = {
    OTC : {"id" : 1, "name" : 'OTC'},
    WEB : {"id" : 2, "name" : 'Web'},
    MOBILE : {"id" : 3, "name" : 'Mobile'},
    TAB : {"id" : 4, "name" : 'Tab'}
  };

  public static statusList = {
    "CREATED" : 0,
    "PENDING" : 1,
    "APPROVED" : 2,
    "CANCELED" : 3,
    "REVERTED" : 4,
    "REJECTED" : 5,
    "SUSPENDED" : 6,
    "BLACKLISTED" : 7,
    "DELETED" : 8,
    "AMENDED" : 9,
    "ACCEPTED" : 10,
    "RELEASED" : 11,
    "DISPATCHED":12,
    "DELIVERED" : 13,
    "DELIVERY_ACCEPTED" : 14,
    "DELIVERY_REJECTED" : 15,
    "RESERVED":17,
    "ON_HOLD":18,
    "AVAILABLE":19,
    "SERVING":20
  };

  public static paymentTypes ={
    1:"Cash",
    2:"Card",
    3:"Visa",
    4:"Master",
    5:"American",
    6:"Voucher",
    7:"Other"
  }

  public static deliveryOption={
    2:"Pickup",
    3:"Deliver"
  }
}
