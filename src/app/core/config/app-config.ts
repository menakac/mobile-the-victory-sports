export class AppConfig {
  //Dev 2.1
  public static GB_API_URL = 'http://124.43.16.202:8056/online-shop';
  public static imagesPath = 'https://s3-ap-southeast-1.amazonaws.com/pankadu-qa/';
  public static shopDetails = {
    "shopID" : 41,
    "branchID" : 54,
    "mainCategoryName" :'category'
  };

  //QA 2.1
  // public static GB_API_URL = 'http://124.43.16.202:8056/online-shop';
  // public static imagesPath = 'https://s3-ap-southeast-1.amazonaws.com/pankadu-qa/';
}
