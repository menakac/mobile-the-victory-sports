import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { IonicModule } from "ionic-angular";
import { CustomNumberIncrementComponent } from './custom-number-increment/custom-number-increment';

@NgModule({
	declarations: [CustomNumberIncrementComponent],
	imports: [IonicModule],
	exports: [CustomNumberIncrementComponent],

  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule {}
