import {Component, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'custom-number-increment',
  templateUrl: 'custom-number-increment.html'
})
export class CustomNumberIncrementComponent implements OnChanges{
  @Input () inputName;
  @Input () inputMin;
  @Input () inputMax;
  @Input () inputValue;
  @Output() submit = new EventEmitter<any>();

  InputData:any = {
    name : this.inputName || '',
    min : this.inputMin || 0,
    max : this.inputMax || 10,
    value : this.inputValue || 0
  };

  constructor() {}

  ngOnInit(){
    this.InputData = {
      name : this.inputName || '',
      min : this.inputMin || 0,
      max : this.inputMax || 100,
      value : this.inputValue || 0
    };
  }

  ngOnChanges(changes: SimpleChanges) {
    this.InputData.value = this.inputValue;
    this.InputValidate(2);
  }

  inputDecrease(){
    this.InputData.value--;
    this.InputValidate(1);
  }

  inputIncrease(){
    this.InputData.value++;
    this.InputValidate(1);
  }

  InputValidate(status){
    let Values = Object.assign({}, this.InputData );

    if(isNaN(Values.value) || !(Values.value)){
      this.InputData.value = Values.min;
    }else if(Values.value < Values.min){
      this.InputData.value = Values.min;
    }else if(Values.value > Values.max){
      this.InputData.value = Values.max;
    }

    if(status==1 || this.InputData.value !== this.inputValue || status==2 && this.InputData.value !== this.inputValue){
      this.submit.emit({name:this.InputData.name,value:this.InputData.value});
    }
  }



}
