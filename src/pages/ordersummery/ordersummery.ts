import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {GlobalData, AppConfig, MainConfig, appOrders} from "../../app/core";
import {HomePage} from "../home/home";


@IonicPage()
@Component({
  selector: 'page-ordersummery',
  templateUrl: 'ordersummery.html',
})
export class OrdersummeryPage {
  orderDetails:any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams,public globalData:GlobalData,public appOrders: appOrders ) {
    if(this.navParams.get('orderDetails')){
      this.orderDetails = navParams.get('orderDetails');
      console.log(this.orderDetails);
      appOrders.cartItems.length = 0;
      for(let product of this.orderDetails.itemList)
      {
        product.image=AppConfig.imagesPath+product.image;

      }
    }else{
      this.navCtrl.setRoot('HomePage').then();
    }
  }

  ionViewDidLoad() {
    }

  goHome()
  {

    this.navCtrl.setRoot('HomePage');
  }

  getDeliveryOption(){
    // deliveryOptionId   paymentTypes paymentTypeId name
    for(let deliveryOption of this.globalData.branch.deliveryOptions){
      if(deliveryOption.deliveryOptionId == this.orderDetails.deliveryOption){
        return deliveryOption.name;
      }
    }

  }

  getPaymentOption(){
    // deliveryOptionId   paymentTypes paymentTypeId name
    for(let paymentOption of this.globalData.branch.paymentTypes){
      if(paymentOption.paymentTypeId == this.orderDetails.paymentOption){
        return paymentOption.name;
      }
    }

  }

}
