import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Md5} from "ts-md5";
import {AdminService} from "../../app/core/services/api-data-services";

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  private registerForm : FormGroup;

  constructor(public navCtrl: NavController, private formBuilder: FormBuilder,
              public toastCtrl: ToastController, private adminService : AdminService, public alertCtrl: AlertController) {
    this.initRegisterForm();
  }

  private initRegisterForm(){
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: [''],
      loginName: ['', [Validators.required]],
      mobile: ['', [Validators.required]],
      email: ['', [Validators.compose([Validators.required, Validators.email])]],
      password: ['', [Validators.required]],
      passwordConfirm: ['', [Validators.required]]
    },{validator: this.matchValidator});
  }

  matchValidator(group: FormGroup) {
    let password = group.controls['password'].value;
    let passwordConfirm = group.controls['passwordConfirm'].value;
    if(password!=='' && passwordConfirm!==''&& password == passwordConfirm){
      return null;
    }
    return {
      mismatch: true
    };
  }

  registerUser(){
    if(this.registerForm.valid){
      let req = {
        "firstName":this.registerForm.controls['firstName'].value,
        "lastName":this.registerForm.controls['lastName'].value,
        "loginName": this.registerForm.controls['loginName'].value,
        "mobile": this.registerForm.controls['mobile'].value,
        "email": this.registerForm.controls['email'].value,
        "password": Md5.hashStr(this.registerForm.controls['password'].value)
      };
      this.adminService.createCustomer(req).then(() => {
        this.presentAlertRegSuccess();
      },() => {
        this.presentToast('User creation unsuccessful! Try again.');
      });
    }else{
      this.presentToast('Form has errors');
    }
  }

  presentToast(message){
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present().then();
  }

  presentAlertRegSuccess() {
    let alert = this.alertCtrl.create({
      title: 'Successful!',
      subTitle: 'User Created Successfully! Please Login To Continue.',
      buttons: [{
        text: 'Ok',
        role: 'ok',
        handler: () => {
          this.navCtrl.setRoot('LoginPage').then();
        }
      }],
      enableBackdropDismiss:false
    });
    alert.present().then();
  }
}
