import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AdminService, AppConfig, GlobalData, UserService, appOrders} from "../../app/core";
import {DatePipe} from "@angular/common";
import {HomePage} from "../home/home";

@IonicPage()
@Component({
  selector: 'page-delivery',
  templateUrl: 'delivery.html',
})
export class DeliveryPage {
  checkOutForm:any={};
  CityList:any = [];
  CountryList:any = [];

  constructor(public navCtrl: NavController, public userService:UserService, public navParams: NavParams,public formBuilder:FormBuilder,
              public adminService:AdminService,public globalData:GlobalData, public alertCtrl: AlertController, public appOrders: appOrders,
              public toastCtrl: ToastController, private datePipe: DatePipe) {
    console.log(globalData.branch);
    this.initCheckoutForm();
    this.getMaxDate();
  }

  ionViewDidLoad() {
    this.getCountries();
    this.getCities();

    if (this.userService.user.isAuthorized) {
      this.checkOutForm.patchValue({
        Name:this.userService.user.firstName?this.userService.user.firstName:''+' '+this.userService.user.lastName?this.userService.user.lastName:'',
        Telephone:this.userService.user.mobile?this.userService.user.mobile:'',
        Email:this.userService.user.email?this.userService.user.email:'',
      });
    }
  }

  initCheckoutForm(){
    this.checkOutForm = this.formBuilder.group({
      Name: ['', [Validators.required]],
      Telephone: ['', [Validators.required]],
      Email: ['', [Validators.compose([
          Validators.pattern('^$|^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])]],
      PaymentType:[(this.globalData.branch.paymentTypes[0].paymentTypeId),[Validators.compose([
        Validators.pattern('[^0]+')
      ])]],
      DeliveryOption:[(this.globalData.branch['deliveryOptions'][0].deliveryOptionId)],
      Country:[0],
      City:['0'],
      Address:[''],
      ExpectedDate:[''],
      ExpectedTime:[''],
      Comments:[''],
    });
  }

  getCountries(){
    this.adminService.getCountries().then((data:any)=>{
      // console.log(data);
    });
  }

  getCities(){
    this.adminService.getCities().then((data:any)=>{
      // console.log(data);
    });
  }

  placeOrder(){
    console.log(this.checkOutForm.controls);
    if(this.formValidationChecker()){
      let alert = this.alertCtrl.create({
        title: 'Confirm Order Placing',
        message: 'Do you want to place the order?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {}
          },
          {
            text: 'Place Order',
            handler: () => {
              this.submitOrder();
            }
          }
        ]
      });
      alert.present().then();
    }else{
      this.presentToast('Fill all the required fields.');
    }
  }

  submitOrder(){
    let req:any = {
      'customerId':this.userService.user.mobile==this.checkOutForm.controls['Telephone'].value?this.userService.user.customerId:0,
      'customerName': this.checkOutForm.controls['Name'].value,
      'contact': this.checkOutForm.controls['Telephone'].value,
      'paymentOption': this.checkOutForm.controls['PaymentType'].value,
      'email': this.checkOutForm.controls['Email'].value!==''?this.checkOutForm.controls['Email'].value:null,
      'comments':this.checkOutForm.controls['Comments'].value!==''?this.checkOutForm.controls['Comments'].value:null,
      'deliveryOption':this.checkOutForm.controls['DeliveryOption'].value,

      'shippingCountry':0,
      'shippingCity':null,
      'address':null,
      'expectedTime':null,
    };

    this.appOrders.placeOrder(req).then((data:any)=>{
      this.presentToast('Order placed successfully!');
      this.navCtrl.setRoot('OrdersummeryPage',{'orderDetails' : data}).then();
    },()=>{
      this.presentToast('Order placement failed! Please try again.');
    });
  }

  formValidationChecker(){
    let status = false;
    if(this.checkOutForm.controls['DeliveryOption'].value == '3'){
      if(this.checkOutForm.controls['Country'].value!=='0' && this.checkOutForm.controls['Address'].value!==''){
        status = true;
      }
    }else if(this.checkOutForm.valid){
      status = true;
    }
    return status;
  }

  presentToast(message) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present().then();
  }

  getMinDate(){
    let minDate = this.datePipe.transform(new Date(),'yyyy-MM-dd');
    return minDate;
  }

  getMaxDate(){
    let maxDate = this.datePipe.transform(new Date().setFullYear((new Date()).getFullYear()+5), 'yyyy-MM-dd') ;
    return maxDate;
  }

  goToHome(){
    this.navCtrl.setRoot('HomePage').then();
  }
}
