import {Component, NgZone, ViewChild} from '@angular/core';
import {
  ActionSheetController, Content, InfiniteScroll, IonicPage, MenuController, ModalController, NavController,
  Slides
} from 'ionic-angular';
import {ProductService, AppConfig, MainConfig, GlobalData, UserService, appOrders} from "../../app/core";
import {CategoryFiltersPage} from "../category-filters/category-filters";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Content) content: Content;
  @ViewChild('categorySlider1') categorySlider1: Slides;
  @ViewChild('categorySlider2') categorySlider2: Slides;

  mainSlider:any = [];
  categories:any = [];
  subCategories:any = [];
  orderBy:any ={
    "key" : 'name',
    "order" : 'asc'
  };
  pagination:any = {
    "offset" : 0,
    "limit" : 10,
  };
  init_pagination:any = Object.assign({},this.pagination);
  stickIt:boolean=false;
  reInfiniteScroll:any;
  categoryFilter:any ={};
  productDetails:Array<any> = [];
  filteredMain:boolean=false;
  filteredOther:boolean=false;

  constructor(public navCtrl: NavController, private productService: ProductService,public globalData: GlobalData,public userService:UserService,
              public actionSheetCtrl: ActionSheetController, public zone: NgZone, public modalCtrl: ModalController, public appOrders:appOrders,
              public menuCtrl:MenuController) {
    this.initPage();

    }

  initPage(){
    this.getProductCategories().then(()=>{
      for(let image of this.globalData.shop.details['banners']){
        let fullImagePath = AppConfig.imagesPath + image.image;
        this.mainSlider.push(fullImagePath);
      }
      this.initProductsList();
    });
  }

  private getProductCategories(){
    let promise = new Promise((resolve, reject) => {
      this.productService.getProductCategory().then((response:any) => {
        for(let category of response){
          category.selected = false;
          if(category.image){
            category.image = AppConfig.imagesPath + category.image;
          }
          if(category.parentCategoryId == 0){
            if(!this.categoryFilter[category.type]){
              this.categoryFilter[category.type] = category.categoryId;
            }
            this.categories.push(category);
          }else{
            this.subCategories.push(category);
          }
        }
        resolve();
      },() => {
        reject();
      });
    });
      return promise;
  }

  selectedToggle(item,parent){
    for(let subCategory of this.subCategories){
      if(subCategory.parentCategoryId == parent.categoryId && subCategory.categoryId !== item.categoryId){
        subCategory.selected = false;
      }
    }
    item.selected = !item.selected;
    this.filteredMain = item.selected;
    this.initProductsList();
  }

  filterCategories(categories,catType,catName){
    // catType 1-mainCategory, 2-otherCategories, 3-subCategories
    let filteredCategories =  categories.filter(function(category) {
      if(catType==1){
        return category.type == AppConfig.shopDetails.mainCategoryName;
      }else if(catType==2){
        return category.type !== AppConfig.shopDetails.mainCategoryName;
      }else if(catType==3){
        return category.type == catName;
      }
    });
    return filteredCategories;
  }

  initProductsList(){
    this.resetPagination().then(()=>{
      let req = {};
      this.getProducts(req,this.reInfiniteScroll?this.reInfiniteScroll:null);
    });
  }

  private getProducts(req,infiniteScroll) {
    req.channel = this.globalData.device.channel;
    req.shopId = AppConfig.shopDetails.shopID;
    req.branchId = AppConfig.shopDetails.branchID;
    req.limit = this.pagination.limit;
    req.offset = this.pagination.offset;
    req.orderByKey = this.orderBy.key;
    req.orderByValue = this.orderBy.order;
    req.statuses = [
      MainConfig.statusList.APPROVED,
    ];
    req.searchKeys= [];
    req.operators=[];
    req.values=[];

    let selectedCategories = [];
    let selectedCategoryNames = '';

    for(let subCategory of this.subCategories){
      if(subCategory.parentCategoryId !== 0 && subCategory.selected && !(selectedCategories.indexOf(subCategory.categoryId) > -1)){
        selectedCategories.push(subCategory.categoryId);
        selectedCategoryNames = selectedCategoryNames + subCategory.categoryId + ',';
      }
    }

    if(selectedCategoryNames!==''){
      selectedCategoryNames = selectedCategoryNames.replace(/,\s*$/, "");
      req = this.addSearchKeyValuesToRequest(req, "categoryId", "in", selectedCategoryNames);
    }

    this.productService.SearchProductsByCategoryBrand(req).then((response: any) => {
      if(this.pagination.offset == this.init_pagination.offset){
        this.productDetails.length = 0;
      }
      if(response && response.data && response.data.length > 0){
        let itemList  = response.data;
        this.getProductPriceList(itemList).then((pricedItems:any)=>{
          for(let product of pricedItems){
            for(let item of product.products){
              item.quantity = this.appOrders.itemLoadingCartCheck(item.productId)?this.appOrders.itemLoadingCartCheck(item.productId):0;
              if(this.appOrders.itemLoadingCartCheck(item.productId)){
                this.appOrders.productToCart(item);
              }
            }
            product.selected = product.products[0].productId;
            this.productDetails.push(product);
          }
          if(infiniteScroll){
            infiniteScroll.complete();
            infiniteScroll.enable(true);
          }
        });
      }else{
        if(infiniteScroll){
          infiniteScroll.complete();
          infiniteScroll.enable(false);
        }
      }
    });
  }

  private addSearchKeyValuesToRequest(req,key,operator,value) {
    req.searchKeys.push(key);
    req.operators.push(operator);
    req.values.push(value);
    return req;
  }

  getProductPriceList(itemList){
    let promise = new Promise((resolve, reject) => {
      let req:any = {
        "shopId" : AppConfig.shopDetails.shopID,
        "branchId" : AppConfig.shopDetails.branchID,
        "itemList" : []
      };
      for(let product of itemList){
        for(let item of product.products){
          let productItem = {
            "productId": item.productId,
            "quantity": 0
          };
          req.itemList.push(productItem);
        }
      }
      this.productService.calculatePrice(req).then((response:any)=>{
        if(response && response.status == MainConfig.responseSuccessStatus){
          for(let savedItem of itemList){
            for(let product of savedItem.products){
              for(let item of response.orderDetails.itemList){
                if(product.productId == item.productId){
                  if (item.image){
                    item.image = AppConfig.imagesPath + item.image;
                  }
                  Object.assign(product,item);
                }
              }
            }
          }
          resolve(itemList);
        }
      },()=>{
        reject();
      });
    });
    return promise;
  }

  sortByActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Sort By',
      buttons: [
        {
          text: 'Ascending (A - Z)',
          handler: () => {
            this.orderBy.key = 'name';
            this.orderBy.order = 'asc';
            this.initProductsList();
          }
        },{
          text: 'Descending (Z - A)',
          handler: () => {
            this.orderBy.key = 'name';
            this.orderBy.order = 'desc';
            this.initProductsList();
          }
        },
      ]
    });
    actionSheet.present().then();
  }

  affixCheck(e){
    this.zone.run(()=>{
      this.stickIt = e.scrollTop > 132;
    });
  }

  slideChanged(){
    let currentIndex = this.stickIt?this.categorySlider1.getActiveIndex():this.categorySlider2.getActiveIndex();
    this.categorySlider1.slideTo(currentIndex);
    this.categorySlider2.slideTo(currentIndex);
  }

  loadMore(infiniteScroll:InfiniteScroll){
    this.reInfiniteScroll = infiniteScroll;
    this.pagination.offset += this.pagination.limit;
    let req = {};
    this.getProducts(req,infiniteScroll);
  }

  private resetPagination(){
    let promise = new Promise((resolve, reject) => {
      this.content.scrollToTop().then(() => {
        this.pagination = Object.assign({},this.init_pagination);
        resolve();
      });
    });
    return promise;
  }

  openFilters(){
    let data:any = {
      'otherCategories' : []
    };
    for(let category of this.filterCategories(this.categories, 2, '')){
      data.otherCategories.push(category);
      data[category.type] = [];
      for(let subCategory of this.filterCategories(this.subCategories, 2, '')){
        if(subCategory.parentCategoryId==category.categoryId){
          data[category.type].push(subCategory);
        }
      }
    }

    let profileModal = this.modalCtrl.create(CategoryFiltersPage,
      {'filterData' :data},
      { enableBackdropDismiss: false
      });
    profileModal.onDidDismiss(data => {
      if(data.apply){
        let otherFilters:boolean = false;
        for(let key in data.data){
          if(data.data[key] > 0){
            otherFilters = true;
          }
        }
        this.filteredOther = otherFilters;

        this.applyFilters(data).then(()=>{
          this.initProductsList();
        },()=>{});
      }
    });
    profileModal.present().then();
  }

  applyFilters(data){
    let loadStatus:boolean = false;
    let promise = new Promise((resolve,reject) => {
      for(let key in data.data){
        for(let subCategory of this.filterCategories(this.subCategories,2,'')){
          if(subCategory.type == key){
            if(data.data[key] == subCategory.categoryId){
              if(!subCategory.selected){
                subCategory.selected = true;
                loadStatus = true;
              }
            }else{
              subCategory.selected = false;
              loadStatus = true;
            }
          }
          // if(subCategory.type == key && data.data[key] == 0 && subCategory.selected){
          //   subCategory.selected = false;
          //   loadStatus = true;
          // }else if(subCategory.type == key && subCategory.categoryId == data.data[key] && !subCategory.selected){
          //   subCategory.selected = true;
          //   loadStatus = true;
          // }
        }
      }

      if(loadStatus){
        resolve();
      }else{
        reject();
      }
    });
    return promise;
  }

  clearFilters(){
    for(let subCategory of this.subCategories){
      subCategory.selected = false;
    }
    this.filteredMain=false;
    this.filteredOther=false;
    this.initProductsList();
  }

  openDetailsPage(productItem){
    this.content.scrollToTop().then();
    this.navCtrl.push('DetailsPage', {'itemDetails' : productItem}).then();
  }

  incrementSubmit(e,product) {
    product.quantity=e.value;
    this.appOrders.productToCart(product);
    if(this.menuCtrl.isOpen('menu2'))
    {
      console.log('menu2')
      this.menuCtrl.enable(true,'menu2');
    }
  }


}
