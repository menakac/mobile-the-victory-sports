import { Component } from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {AppConfig, MainConfig} from "../../app/core/config";
import {ProductService} from "../../app/core/services/api-data-services";
import {InquirymodelPage} from "../inquirymodel/inquirymodel";
import {appOrders} from "../../app/core/classes";

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {
  item:any = {};
  images:any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,public productService:ProductService,public modalCtrl:ModalController,
              public appOrders:appOrders) {

    if(this.navParams.get('itemDetails')){
      this.item = navParams.get('itemDetails');
    }else{
      this.navCtrl.setRoot('HomePage').then();
    }

  }

  ionViewDidLoad(){
    this.initProductDetails();
  }


  private initProductDetails() {
    let req = {
      'shopId': AppConfig.shopDetails.shopID,
      'productId': this.item.productId,
    };

    this.productService.getProductDetails(req).then((response: any) => {
      if(response && response.status==MainConfig.responseSuccessStatus && response.data.length > 0){
        for (let key in this.item){
          if(this.item[key] == null){
            delete this.item[key];
          }
        }
        this.item = Object.assign(response.data[0],this.item);
        if(this.item.image){
          this.images.push(this.item.image);
        }
        for(let key in this.item.additionalData.imageArray){
          this.item.additionalData.imageArray[key] = AppConfig.imagesPath + this.item.additionalData.imageArray[key];
          this.images.push(this.item.additionalData.imageArray[key]);
        }
      }
    },()=>{
      this.navCtrl.setRoot('HomePage').then();
    })

  }

  sendinquiry(productId,groupId,name) {
    let data = {
      productId:productId,
      groupId:groupId,
      name:name
    };
    const modal = this.modalCtrl.create(InquirymodelPage,{product : data}, { enableBackdropDismiss: true });
    modal.present().then();
  }

  public incrementSubmit(e,product) {
    this.item.quantity=e.value;
    this.appOrders.productToCart(product);
  }
}

