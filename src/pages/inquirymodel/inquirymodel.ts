import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Md5} from "ts-md5";
import {AppConfig, MainConfig} from "../../app/core/config";
import {AdminService, ProductService} from "../../app/core/services/api-data-services";
import {UserService} from "../../app/core/services";

/**
 * Generated class for the InquirymodelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-inquirymodel',
  templateUrl: 'inquirymodel.html',
})
export class InquirymodelPage {

  private inquiryForm : FormGroup;

  product:any={};
  constructor(public navCtrl: NavController, public navParams: NavParams,public formBuilder:FormBuilder,
              public produtService:ProductService,public alertCtrl:AlertController,public adminService:AdminService,public userService:UserService) {
    this.product = navParams.get('product');
    this.initinquiryForm();
  }



  ionViewDidLoad() {
    if (this.userService.user.isAuthorized) {
      this.getCustomerDetails();
    }
  }



  private initinquiryForm(){
    this.inquiryForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      mobile: ['', [Validators.required]],
     // email: ['', [Validators.compose([ Validators.email])]],
      email:[''],
      comment:['',[Validators.required]]
    });
    }

  sendInquiry()
  {
    if(this.inquiryForm.valid){
      let req = {
        "channel": MainConfig.CHANNELS.MOBILE.id,
        "shopId": AppConfig.shopDetails.shopID,
        "branchId": AppConfig.shopDetails.branchID,
        "name":this.inquiryForm.controls['name'].value,
        "contact":this.inquiryForm.controls['mobile'].value,
        "email":this.inquiryForm.controls['email'].value,
        "groupId":this.product.groupId,
        "productId":this.product.productId
        };
      this.produtService.SubmitInquiry(req).then(() => {

        let alert = this.alertCtrl.create({
          title: 'Thank You!',
          subTitle: 'Your Inquiry has been successfully sent.We will contack you very soon!',
          buttons: [{
            text: 'Ok',
            handler: () => {
              this.navCtrl.pop()
            }}]
        });
        alert.present();

      },() => {

      });
    }
  }



  getCustomerDetails(){
    let req = {
      "shopId": AppConfig.shopDetails.shopID
    };
    this.adminService.getCustomerDetails(req).then((response:any) => {
      if(response.data){

        this.inquiryForm.patchValue({
          name:response.data[0].firstName,
          email:response.data[0].email,
          mobile:response.data[0].mobile,

        });



      }
    },(error) => {});
  }

  gotodetails()
  {
    this.navCtrl.pop();
  }

}
