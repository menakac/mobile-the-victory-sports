import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InquirymodelPage } from './inquirymodel';

@NgModule({
  declarations: [
    InquirymodelPage,
  ],
  imports: [
    IonicPageModule.forChild(InquirymodelPage),
  ],
})
export class InquirymodelPageModule {}
