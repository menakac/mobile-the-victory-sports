import { Component } from '@angular/core';
import {InfiniteScroll, IonicPage, NavController, NavParams} from 'ionic-angular';
import {GlobalData} from "../../app/core/data";
import {AppConfig, MainConfig} from "../../app/core/config";
import {AdminService} from "../../app/core/services/api-data-services";
import {UserService} from "../../app/core/services";
import {appOrders} from "../../app/core/classes";

/**
 * Generated class for the MybookingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mybooking',
  templateUrl: 'mybooking.html',
})
export class MybookingPage {

  branchDetails:any;
  bookingList:Array<any> = [];

  pagination:any = {
    "offset" : 0,
    "limit" : 10,
    "recordCount" : 0
  };
  reActiveInfinite: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private userService : UserService,
              private adminService :AdminService, private globalData: GlobalData,private appOrders:appOrders) {
    this.branchDetails = this.globalData.branch;
  }

  ionViewDidLoad() {
    if(this.userService.user.isAuthorized){
      this.init();
    }else{
      this.userService.getLocalAuthResponse().then((response:any) => {
        if(response){
          this.userService.setLocalAuthResponse(response,false);
          if(this.userService.user.isAuthorized){
            this.init();
          }else{
            this.navCtrl.setRoot('LoginPage');
          }
        }else{
          this.navCtrl.setRoot('LoginPage');
        }
      },(error) => {
      })
    }
  }

  init(){
    let req = {};
    this.getCustomerOrders(req, this.reActiveInfinite);
  }

  private getCustomerOrders(req,infiniteScroll){
    req.channel = this.globalData.device.channel;
    req.shopId = AppConfig.shopDetails.shopID;
    req.branchId = AppConfig.shopDetails.branchID;
    req.limit = this.pagination.limit;
    req.offset = this.pagination.offset;
    req.orderByKey='reference';
    req.orderByValue='desc';
    req.statuses = [
      MainConfig.statusList.PENDING,
      MainConfig.statusList.APPROVED,
      MainConfig.statusList.SUSPENDED
    ];



    this.adminService.findCustomerOrders(req).then((response:any) => {
      if(response && response.data && response.data.length > 0){
        for(let booking of response.data){
          booking.finalValue=booking.finalValue.toLocaleString("en");
          this.bookingList.push(booking);
        }
        if(infiniteScroll){
          infiniteScroll.complete();
          if(response.data.length < this.pagination.limit || response.data.length == this.pagination.recordCount){
            infiniteScroll.enable(false);
          }
        }
        // let time = this.datePipe.transform(response.data[0].createdDate, 'yyyy-MM-dd');
      }else{
        if(infiniteScroll){
          infiniteScroll.complete();
          infiniteScroll.enable(false);
        }
      }
    },(error) => {});
  }

  loadMore(infiniteScroll: InfiniteScroll){
    this.reActiveInfinite = infiniteScroll;
    this.pagination.offset += this.pagination.limit;
    let req = {};
    this.getCustomerOrders(req,infiniteScroll);
  }
}
