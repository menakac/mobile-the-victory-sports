import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {AppConfig} from "../../app/core/config";
import {AdminService} from "../../app/core/services/api-data-services";
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";
import {UserService} from "../../app/core/services";
import {GlobalData} from "../../app/core/data";
import {Md5} from "ts-md5";

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  private customerForm : FormGroup;
  private changePasswordForm: FormGroup;
  profileEdit:boolean=false;
  passwordEdit:boolean=false;
  valid:boolean=false;
  public customerDetails:any=[];

  constructor(public navCtrl: NavController, public navParams: NavParams,public adminService: AdminService,
              private formBuilder: FormBuilder,private userService:UserService,public globalData:GlobalData,
              public toastCtrl: ToastController,) {
    this.initcustomerForm();
    this.initChangePasswordForm();
  }

  ionViewDidLoad() {
    if(this.userService.user.isAuthorized){
      this.init();
    }else{
      this.userService.getLocalAuthResponse().then((response:any) => {
        if(response){
          this.userService.setLocalAuthResponse(response,false);
          if(this.userService.user.isAuthorized){
            this.init();
          }else{
            this.navCtrl.setRoot('LoginPage');
          }
        }else{
          this.navCtrl.setRoot('LoginPage');
        }
      },(error) => {
      })
    }

  }
  init(){
    this.getCustomerDetails();
}

  private initcustomerForm(){
    this.customerForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: [''],
      username:[''],
      mobile: ['', [Validators.compose(
        [
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10)
        ]
      )]],
      email: ['', [Validators.compose([Validators.required, Validators.email])]],
    });
  }

  editProfile(){
    this.profileEdit = true;
  }

  cancelEditProfile(){
    this.profileEdit = false;
  }

  editPassword(){
    this.passwordEdit = true;
  }

  cancelEditPassword(){
    this.passwordEdit = false;
  }

  initChangePasswordForm(){
    this.changePasswordForm = this.formBuilder.group({
      currentPassword : ['',[Validators.compose(
        [
          Validators.required,
          Validators.maxLength(8)
        ]
      )]],
      newPassword : ['',[Validators.compose(
        [
          Validators.required,
          Validators.maxLength(8)
        ]
      )]],
      confirmNewPassword : ['', Validators.compose(
        [
          Validators.required,
          Validators.maxLength(8)
        ]
      )]
    });
  }


  private getCustomerDetails(){
    let req = {
      "shopId": AppConfig.shopDetails.shopID
    };
    this.adminService.getCustomerDetails(req).then((response:any) => {
      if(response.data){
        this.customerForm.patchValue({
          firstName:response.data[0].firstName,
          lastName:response.data[0].lastName,
          email:response.data[0].email,
          mobile:response.data[0].mobile,
          username:this.userService.user.loginName,

        });



      }
    },(error) => {});
  }

  presentToast(message){
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }

  updateUser()
  {
    if(this.valid==false)
    {
      this.valid=true;
    }
    else
    {
      this.valid=false
    }


    if(this.customerForm.valid){
      let req = {
        "channel": this.globalData.device.channel,
        "shopId": AppConfig.shopDetails.shopID,
        "branchId": AppConfig.shopDetails.branchID,
        "customerId": this.userService.user.customerId,
        "firstName":this.customerForm.controls['firstName'].value,
        "middleName": this.customerDetails.middleName,
        "lastName":this.customerForm.controls['lastName'].value,
        "address1": this.customerDetails.address1,
        "address2": this.customerDetails.address2,
        "city": this.customerDetails.city,
        "countryId": this.customerDetails.countryId,
        "mobile": this.customerForm.controls['mobile'].value,
        "fax": this.customerDetails.fax,
        "email":this.customerForm.controls['email'].value,
        "dateOfBirth": this.customerDetails.dateOfBirth
      };
      this.adminService.updateCustomer(req).then((response:any) => {
        if(response && response.status==1){
          this.profileEdit = false;
          this.getCustomerDetails();
          this.presentToast('User Details Updated Successfully!');
        }
      },(error) => {});
    }else{
     // this.presentToast('Form has errors');
    }
  }

  changePassword()
  {

    if(this.valid==false)
    {
      this.valid=true;
    }
    else
    {
      this.valid=false
    }

    if(this.changePasswordForm.valid){
      if(this.changePasswordForm.value.newPassword == this.changePasswordForm.value.confirmNewPassword){
        let req = {
          "channel":this.globalData.device.channel,
          "shopId":AppConfig.shopDetails.shopID,
          "branchId":AppConfig.shopDetails.branchID,
          "primary_id":this.userService.user.customerId,
          "oldPassword": Md5.hashStr(this.changePasswordForm.controls['currentPassword'].value),
          "newPassword": Md5.hashStr(this.changePasswordForm.controls['newPassword'].value),
          "sessionId":this.userService.user.sessionId
        };

        this.adminService.changePassword(req).then((response:any) => {
          if(response && response.status == 1){
            this.presentToast('Password Changed Successfully!');
            this.passwordEdit = false;
            this.initChangePasswordForm();
          }
        },(error) => {});

      }else{
        this.presentToast('New Password Confirmation Failed');
      }
    }else{
      //this.presentToast('Form has errors');
    }
  }

}
