import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AdminService} from "../../app/core/services/api-data-services";

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  private resetPasswordForm : FormGroup;

  constructor(private formBuilder: FormBuilder, public navCtrl: NavController, public toastCtrl: ToastController,
              private adminService : AdminService, public navParams: NavParams) {
    this.initPasswordResetForm();
  }

  initPasswordResetForm(){
    this.resetPasswordForm = this.formBuilder.group({
      loginName: ['', [Validators.required]],
      email: ['', [Validators.compose([Validators.required, Validators.email])]]
    });
  }

  resetPassword(){
    if(this.resetPasswordForm.valid){

      let req = {
        'loginName':this.resetPasswordForm.controls['loginName'].value,
        'email':this.resetPasswordForm.controls['email'].value
      };

      this.adminService.resetPassword(req).then((response:any) => {
        if(response && response.status == 1){
          this.presentToast('Reset Successful! Please check your Email.');
          this.navCtrl.setRoot('LoginPage').then();
        }
      },() => {
        this.presentToast('Reset Failed! Please try again.');
      });

    }else{
      this.presentToast('Username/ Email Not Valid');
    }
  }

  presentToast(message){
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present().then();
  }

  ionViewDidLoad() {}

}
