import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoryFiltersPage } from './category-filters';

@NgModule({
  declarations: [
    CategoryFiltersPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoryFiltersPage),
  ],
})
export class CategoryFiltersPageModule {}
