import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-category-filters',
  templateUrl: 'category-filters.html',
})
export class CategoryFiltersPage {

  filterData:any;
  selectedFilters:any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.filterData = JSON.parse(JSON.stringify(navParams.get('filterData')));
    for(let category of this.filterData.otherCategories){
      this.selectedFilters[category.type] = 0;
      for(let subCategory of this.filterData[category.type]){
        if(subCategory.selected){
          this.selectedFilters[category.type] = subCategory.categoryId;
        }
      }
    }
  }

  ionViewDidLoad() {}

  filterChange(e,subCategory){
    for(let subcategory of this.filterData[subCategory]){
      subcategory.selected = subcategory.categoryId == e;
    }
  }

  dismiss(apply) {
    this.viewCtrl.dismiss({'apply':apply,'data':this.selectedFilters}).then();
  }

}
