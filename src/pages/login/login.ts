import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AppConfig, MainConfig} from "../../app/core/config";
import {Md5} from "ts-md5";
import {AdminService} from "../../app/core/services/api-data-services";
import {UserService} from "../../app/core/services";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private loginForm : FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder,
              private adminService : AdminService, private userService : UserService, public toastCtrl: ToastController) {
    this.initLoginForm();
  }

  ionViewDidLoad() {
    this.userService.getLocalAuthResponse().then((response:any) => {
      if(response.sessionId){
        this.userService.setLocalAuthResponse(response,false).then(()=>{
          this.navCtrl.setRoot('HomePage').then();
        },()=>{});
      }
    },() => {});
  }

  initLoginForm(){
    this.loginForm = this.formBuilder.group({
      userName: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  authenticateUser(){
    if(this.loginForm.valid){
      let req = {
        "channel": MainConfig.CHANNELS.MOBILE.id,
        "shopId": AppConfig.shopDetails.shopID,
        "branchId": AppConfig.shopDetails.branchID,
        "loginName": this.loginForm.controls['userName'].value,
        "password": Md5.hashStr(this.loginForm.controls['password'].value)
      };
      this.adminService.login(req).then((authResponse:any) => {
        if(authResponse.status == MainConfig.responseSuccessStatus){
          this.userService.setLocalAuthResponse(authResponse,true).then(()=>{
            this.navCtrl.setRoot('HomePage').then();
          },()=>{});
        }
      },()=>{
        this.presentToast('Invalid Username/ Password');
      });
    }else{
      this.presentToast('Form has errors');
    }
  }

  presentToast(message){
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present().then();
  }

  rootPage(page){
    this.navCtrl.setRoot(page).then();
  }

}
